# ProtoFox

This is a mix of CSS customizations to make Firefox more to my liking.  
It requires FF89+ with the Proton UI.

## A rough list of changes

- all the internal browser pages have dark theme (big thanks to Quantum Nox!)
- the whole UI is more compact
- the tab bar is gone (press alt to temporarily restore it, go to View -> Toolbars -> Menu Bar to enable it for longer)
- many small visual tweaks

## Design philosophy

### Proton looks pretty slicc, just a bit too thicc.

The parts of UiFix I use here mostly solve that issue. I made some tweaks of my own as well.

I very much prefer seeing a small icon when music plays than a big "PLAYING" which makes the tab 2x taller than it needs to be. This is reverted to something akin to Photon here.  
There's also icons UiFix readded in some menus, and I love those.

### Dark theme is love, dark theme is life

I use Quantum Nox's `userContent.css` because it's amazing at turning *all* of the internal browser pages dark.  
The colors don't exactly match Proton, but it's way better than being blinded by them.

### It doesn't need to be perfect

This is far from my main project, so it's an attempt at getting the best final result with the least work.  
That's why I'm using some other themes as a basis instead of meticulously picking colors for everything myself.  
Big thanks to the maintainers of those, it's a lot of work theming a whole web browser.  

### Tree Style Tab extension > standard horizontal tabs

When I tried it I immediately loved it. I also noticed the normal tab bar was redundant. So I got rid of it.  
I still keep a good looking style of those tabs though.

## Licensing and Credits

- My code (files in the root of the repo) is available under MPL 2.0
- Code from [Quantum Nox Firefox Dark Full Theme](https://github.com/Izheil/Quantum-Nox-Firefox-Dark-Full-Theme) (quantumnox folder) is available under MPL 2.0
- Code from [Firefox Ui Fix](https://github.com/black7375/Firefox-UI-Fix) (uifix folder) doesn't have a license currently. I hope that it's usage here with proper credit is okay though :)
